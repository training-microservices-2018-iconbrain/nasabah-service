package com.permana.brain.wilson.training.microservice.nasabahservice.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.permana.brain.wilson.training.microservice.nasabahservice.entitiy.Nasabah;

public interface NasabahDao extends PagingAndSortingRepository<Nasabah,Long> {
	
}
