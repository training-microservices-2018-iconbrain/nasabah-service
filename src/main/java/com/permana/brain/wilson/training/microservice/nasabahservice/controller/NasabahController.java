package com.permana.brain.wilson.training.microservice.nasabahservice.controller;

import org.springframework.web.bind.annotation.RestController;

import com.permana.brain.wilson.training.microservice.nasabahservice.dao.NasabahDao;
import com.permana.brain.wilson.training.microservice.nasabahservice.entitiy.Nasabah;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NasabahController {
	
	@Autowired private NasabahDao nasabahDao;
	
	@GetMapping("/nasabah/")
	public Page<Nasabah> ambilDataNasabah(Pageable page) {
		return nasabahDao.findAll(page);
	}
	
}


