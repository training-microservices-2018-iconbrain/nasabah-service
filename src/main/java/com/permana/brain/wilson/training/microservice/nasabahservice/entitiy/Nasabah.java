package com.permana.brain.wilson.training.microservice.nasabahservice.entitiy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Data
public class Nasabah {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
	
	@NotNull @NotEmpty
	private String nama;
	
	@NotNull @NotEmpty @Email
	private String email;
	
	@NotNull @NotEmpty
	private String noHp;
	
	
}
